<?php

    /*
     *  Function    : generateSentence()
     *
     *  Description : Generate a sentence listing out all items provided in an array.
     *
     *  Parameters  : (string)               start         - The start of the sentence
     *                (array[string])        items         - The list of items
     *                (string/array[string]) append        - (optional) item types to be appended to each item
     *
     *  Return      : string
     *
     *  Example     : See run.php for input/output examples
     *
     *  Test        : Execute run.php to test your implementation.
     *                (In console, simply run the command 'php run.php')
     *
     */


    function generateSentence($start, $items, $append = null){
        $count = 0;
        $sentence = $start . ': ';
        foreach( $items as $item){
          if ($item === end($items)){
           $sentence = substr( $sentence, 0, -2) . ' and ' .$item ;
           if ($append !== null){
             is_array($append) ? $sentence .= ' ' . $append[$count] : $sentence .= ' ' . $append;
           }
           $sentence .= '.';
         }else{
           $sentence .= $item;
           if ($append !== null){
             is_array($append) ? $sentence .= ' ' . $append[$count] : $sentence .= ' ' . $append;
           }
           $sentence .= ", ";
         }
         $count++;
        }
        return $sentence;
    }
